package com.asurion.iot.nlp.helper;

import java.util.HashMap;
import java.util.Map;

public class ResponseHelper {
	
	private static Map<String,String> responseMap = new HashMap<String,String>();
	
	static {
		
		responseMap.put("device", "Can you tell me what device?");
		responseMap.put("application", "Can you tell me for what applications need help?");
		responseMap.put("model", "Need the model to proceed?");
		responseMap.put("manufacturer", "Can you tell the manfacturer of the device?");
	}
	
	public static String getResponse(String key) {
		
		return responseMap.get(key);
	}

}

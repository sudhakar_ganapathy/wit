package com.asurion.iot.nlp.helper;

import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import com.asurion.iot.nlp.dto.Applications;
import com.asurion.iot.nlp.dto.Device;
import com.asurion.iot.nlp.dto.Manufacturer;
import com.asurion.iot.nlp.dto.Model;
import com.asurion.iot.nlp.node.DeviceTree;
import com.asurion.iot.nlp.node.Node;

public class WitSessionHelper {

	public static void startAInteractiveSession(String expression,String intent,
			List<Device> deviceList, List<Manufacturer> manufacturerList,
			List<Model> modelList, List<Applications> applicationList) {
		
		String deviceName = null;
		String modelName = null;
		String manufacturerName = null;
		Node modelNode = null;
		Node manufacturerNode = null;
		Node deviceNode = null;
		DeviceTree tree = loadDeviceTree();
		tree.printLevelOrderDeviceTree(tree.getRoot());
		Node root = tree.getRoot();
		if(modelList != null && !modelList.isEmpty()) {
			for(Model model : modelList) {
				modelNode = tree.findNodeByName(model.getValue(), root);
				if(modelNode != null) {
					 modelName = modelNode.getName();
				}
			}
		}
		if(manufacturerList != null && !manufacturerList.isEmpty()) {
			for(Manufacturer manufacturer : manufacturerList) {
				manufacturerNode = tree.findNodeByName(manufacturer.getValue(), root);
				manufacturerName = manufacturerNode.getName();
			}
		} else if(modelNode != null){
			manufacturerName = modelNode.getParent().getName();
		}
		
		if( deviceList != null && !deviceList.isEmpty()) {
			deviceName = deviceList.get(0).getValue();
			if("Unknown".equalsIgnoreCase(deviceName)) {
				modelName = null;
				manufacturerName = null;
			}
		} else if(modelNode != null) {
			deviceName = modelNode.getParent().getParent().getParent().getName();
		} else if(manufacturerNode != null) {
			deviceName = manufacturerNode.getParent().getParent().getName();
		}
		
		Scanner scanner = new Scanner(System.in);
	//	StringBuffer searchString = new StringBuffer(expression);
		while( StringUtils.isEmpty(deviceName)  || StringUtils.isEmpty(modelName) || StringUtils.isEmpty(manufacturerName)) {
			if(StringUtils.isEmpty(deviceName) || "Unknown".equalsIgnoreCase(deviceName)) {
				String deviceResponse = ResponseHelper.getResponse("device");
				System.out.println(deviceResponse);
				deviceName = scanner.nextLine();
				//searchString.append("  ");
				//searchString.append(deviceName);
			}
			
			if(StringUtils.isEmpty(manufacturerName)) {
				String manufacturerResponse = ResponseHelper.getResponse("manufacturer");
				System.out.println(manufacturerResponse);
				manufacturerName = scanner.nextLine();
				//searchString.append("  ");
				//searchString.append(manufacturerName);
			}
			
			if(StringUtils.isEmpty(modelName)) {
				String modelResponse = ResponseHelper.getResponse("model");
				System.out.println(modelResponse);
				modelName = scanner.nextLine();
				//searchString.append("  ");
				//searchString.append(modelName);
			}
		}
		scanner.close();
		System.out.println("Original Expression::"+ expression);
		System.out.println("Device Name::"+ deviceName);
		System.out.println("Manufacturer name::"+ manufacturerName);
		System.out.println("Model Name::"+ modelName);
		
		
	}
	
	
	
	
	private static DeviceTree loadDeviceTree() {
		
		DeviceTree tree = new DeviceTree();
		tree.createRootNode("Device");
		tree.addToRoot("TV", null, "Device");
		tree.addToRoot("Phone", null, "Device");
		tree.addToRoot("Refridgerator", null, "Device");
		tree.addToRoot("Tablets", null, "Device");
		tree.addToRoot("ios", null, "Phone");
		tree.addToRoot("Android", null, "Phone");
		tree.addToRoot("Samsung", null, "Android");
		tree.addToRoot("HTC", null, "Android");
		tree.addToRoot("Sony", null, "Android");
		tree.addToRoot("Motorola", null, "Android");
		tree.addToRoot("Google", null, "Android");
		tree.addToRoot("Nokia", null, "Android");
		tree.addToRoot("LG", null, "Android");
		tree.addToRoot("Apple", null, "ios");
		tree.addToRoot("Iphone 5", "I5", "Apple");
		tree.addToRoot("Iphone 6", "I6", "Apple");
		tree.addToRoot("Iphone 6 plus", "I6 plus", "Apple");
		tree.addToRoot("Galaxy Note 3", "Note 3", "Samsung");
		tree.addToRoot("Galaxy Note 4", "Note 4", "Samsung");
		tree.addToRoot("Galaxy S5", "S5", "Samsung");
		tree.addToRoot("Galaxy S6", "S6", "Samsung");
		tree.addToRoot("Galaxy S6 Edge", "S6 Edge", "Samsung");
		tree.addToRoot("Galaxy S4 ", "S4", "Samsung");
		tree.addToRoot("Galaxy Note Edge", "Note Edge", "Samsung");
		tree.addToRoot("Moto E", null, "Motorola");
		tree.addToRoot("Moto G LTE", null, "Motorola");
		tree.addToRoot("Droid Turbo", "Droid", "Motorola");
		tree.addToRoot("Moto X", null, "Motorola");
			
		return tree;
	}

}

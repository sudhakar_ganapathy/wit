package com.asurion.iot.nlp.node;

import java.util.ArrayList;
import java.util.List;

public class Node {
	
	private String name;
	private String aliasName;
	private boolean isLeaf = true;
	private List<Node> children = new ArrayList<Node>();
	private Node parent;
	
	public Node(String name) {
		this.name = name;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isLeaf() {
		return isLeaf;
	}
	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}
	public List<Node> getChildren() {
		return children;
	}
	public void setChildren(List<Node> children) {
		this.children = children;
	}
	public Node getParent() {
		return parent;
	}
	public void setParent(Node parent) {
		this.parent = parent;
	}
	
	public String getAliasName() {
		return aliasName;
	}


	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}
	
	
	

}

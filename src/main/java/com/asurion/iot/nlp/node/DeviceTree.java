package com.asurion.iot.nlp.node;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.commons.lang3.StringUtils;

public class DeviceTree {
	private Node root;
	
	
	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	public void createRootNode(String rootName) {
		root = new Node(rootName);
		root.setLeaf(false);
		root.setParent(null);
	}
	
	public void addToRoot(String name,String aliasName, String rootName) {
		
		if(root == null) {
			throw new RuntimeException("Root Node is null");
		}
		
		Node matchedRoot = findNodeByName(rootName,root);
		
		if(matchedRoot == null) {
			throw new RuntimeException("Unable to find the node");
		}
		matchedRoot.setLeaf(false);
		//System.out.println("Adding  "+ name+" to  "+ matchedRoot.getName());
		Node nodeToBeAdded = new Node(name);
		nodeToBeAdded.setParent(matchedRoot);
		if(StringUtils.isNotEmpty(aliasName)) {
			nodeToBeAdded.setAliasName(aliasName);
		}
		List<Node> childrenList = matchedRoot.getChildren();
		if(childrenList == null) {
			childrenList = new ArrayList<Node>();
			matchedRoot.setChildren(childrenList);
		}
		childrenList.add(nodeToBeAdded);
	
	}

	public Node findNodeByName(String rootName,Node startFromNode) {
		Node matchedNode = null;
		
		if(rootName.equalsIgnoreCase(startFromNode.getName()) || rootName.equalsIgnoreCase(startFromNode.getAliasName())) {
			matchedNode = startFromNode;
		} else {
			List<Node> childrenList =  startFromNode.getChildren();
			
			if(childrenList == null || childrenList.isEmpty()) {
				return matchedNode;
			}
			for(Node node : childrenList) {
				matchedNode = findNodeByName(rootName,node);
				if(matchedNode != null) {
					break;
				}
			}
		}
		
		return matchedNode;
	}
	
	private void printTree() {
		printTree(root);
	}
	
	private void printTree(Node root) {
	//	System.out.println("---------------------------------------------------");
	//	System.out.println("Name::"+ root.getName());
		List<Node> childrenList = root.getChildren();
		if(childrenList == null || childrenList.isEmpty()) {
			return;
		}
		for(Node node : childrenList) {
			printTree(node);
		}
		
	}
	
	public void printLevelOrderDeviceTree(Node root) {
		
		System.out.println("------------Printing the Device Tree---------------");
		Queue<Node> deviceQueue  = new LinkedList<Node>();
		deviceQueue.add(root);
		while(!deviceQueue.isEmpty()) {
			Node node = deviceQueue.poll();
			System.out.println(node.getName());
			List<Node> childrenList = node.getChildren();
			if(childrenList == null || childrenList.isEmpty()) {
				continue;
			}
			for(Node childNode : childrenList) {
				deviceQueue.add(childNode);
			}
			
		}
	}
	
	
	public static void main(String argv[]) {
		
		DeviceTree tree = new DeviceTree();
		tree.createRootNode("Device");
		tree.addToRoot("TV", null, "Device");
		tree.addToRoot("Phone", null, "Device");
		tree.addToRoot("Refridgerator", null, "Device");
		tree.addToRoot("Tablets", null, "Device");
		tree.addToRoot("ios", null, "Phone");
		tree.addToRoot("Android", null, "Phone");
		tree.addToRoot("Samsung", null, "Android");
		tree.addToRoot("HTC", null, "Android");
		tree.addToRoot("Sony", null, "Android");
		tree.addToRoot("Motorola", null, "Android");
		tree.addToRoot("Google", null, "Android");
		tree.addToRoot("Nokia", null, "Android");
		tree.addToRoot("LG", null, "Android");
		tree.addToRoot("Apple", null, "ios");
		tree.addToRoot("Iphone 5", "I5", "Apple");
		tree.addToRoot("Iphone 6", "I6", "Apple");
		tree.addToRoot("Iphone 6 plus", "I6 plus", "Apple");
		tree.addToRoot("Galaxy Note 3", "Note 3", "Samsung");
		tree.addToRoot("Galaxy Note 4", "Note 4", "Samsung");
		tree.addToRoot("Galaxy S5", "S5", "Samsung");
		tree.addToRoot("Galaxy S6", "S6", "Samsung");
		tree.addToRoot("Galaxy S6 Edge", "S6 Edge", "Samsung");
		tree.addToRoot("Galaxy S4 ", "S4", "Samsung");
		tree.addToRoot("Galaxy Note Edge", "Note Edge", "Samsung");
		Node node =  tree.findNodeByName("i5",tree.getRoot());
		System.out.println("matched by alias name::"+node.getName());
		tree.printTree();
	}
	
	
	
}

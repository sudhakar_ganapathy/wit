package com.asurion.iot.nlp.main;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.asurion.iot.nlp.dto.Applications;
import com.asurion.iot.nlp.dto.Device;
import com.asurion.iot.nlp.dto.Entity;
import com.asurion.iot.nlp.dto.IntentInfo;
import com.asurion.iot.nlp.dto.Manufacturer;
import com.asurion.iot.nlp.dto.Model;
import com.asurion.iot.nlp.dto.WitResponseWrapper;
import com.asurion.iot.nlp.helper.WitSessionHelper;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WitHttpHandlerUtil {
	
	
	private static String WIT_URL = "https://api.wit.ai/message?v=20150624&";
	private static String AUTH_TOKEN ="VC5E6PUBEJHRDV7QE4ETXWYV3GQY5UYN";
    private static final HttpClient httpClient ;
	
	static {
		 httpClient = HttpClientBuilder.create().build();
		
	}

	public static void main(String[] args) {
		
		if(args.length != 1) {
			throw new IllegalArgumentException("Need to pass an expression");
		}
		if(StringUtils.isEmpty(args[0])) {
			throw new IllegalArgumentException("Expression cannot be empty");
		}
		String expression =  args[0];
		WitResponseWrapper witResponseWrapper = null;
		try {
			witResponseWrapper = getWitResponse(expression);
		} catch (Exception e) {
			throw  new RuntimeException(e);
		}
		Entity entity = witResponseWrapper.getOutcomes().get(0).getEntity();
		String intent = witResponseWrapper.getOutcomes().get(0).getIntent();
		System.out.println("Intent::"+ intent);
		List<Device> deviceList = entity.getDeviceList();
		List<IntentInfo> intentList = new ArrayList<IntentInfo>();
		if(deviceList != null && !deviceList.isEmpty()) {
			for(Device device : deviceList) {
				//IntentInfo intentInfo = new IntentInfo();
				//intentInfo.setDevice(device.getValue());
				System.out.println("Device Name::"+ device.getValue());
				//intentList.add(intentInfo);
				
			}
		}
		List<Applications> applicationList = entity.getAppList();
		if(applicationList != null && !applicationList.isEmpty()) {
			for(Applications apps : applicationList) {
				System.out.println("Application::"+apps.getValue());
			}
		}
		List<Manufacturer> manufacturerList = entity.getManufacturerList();
		if(manufacturerList != null && !manufacturerList.isEmpty()) {
			for(Manufacturer manufacturer : manufacturerList) {
				System.out.println("Manufacturer Name ::"+ manufacturer.getValue());
			}
		}
		List<Model> modelList = entity.getModelList();
		if(modelList != null && !modelList.isEmpty()) {
			for(Model phoneModel : modelList) {
				System.out.println("Model Name::"+ phoneModel.getValue());
			}
		}
			
		
		
		WitSessionHelper.startAInteractiveSession(expression,intent,deviceList,manufacturerList,modelList,applicationList);
	
	//	System.out.println("response="+apiOutput);

	}
	
	public static WitResponseWrapper   getWitResponse(String expression) throws ClientProtocolException, IOException {
		
		String apiOutput = getResponseForExpression(expression);
		ObjectMapper mapper = getObjectMapper();
		WitResponseWrapper witResponseWrapper = mapper.readValue(apiOutput,WitResponseWrapper.class); 
		System.out.println("response="+apiOutput);
		return witResponseWrapper;
		
	}

	

	private static String getResponseForExpression(String expression) throws ClientProtocolException, IOException {
		HttpGet httpGet = new HttpGet(WIT_URL + "q=" +URLEncoder.encode(expression, "UTF-8"));
		
		httpGet.setHeader("Authorization", "Bearer " + AUTH_TOKEN);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response =	 httpClient.execute(httpGet);
		// Check for HTTP response code: 200 = success
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "+ response.getStatusLine().getStatusCode());
		}
		
		return  EntityUtils.toString(response.getEntity());
	}
	
	private static ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		return mapper;
	}
	
	
}

package com.asurion.iot.nlp.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Entity {
	@JsonProperty("Models") 
	List<Model> modelList;
	
	@JsonProperty("Manufacturer") 
	List<Manufacturer> manufacturerList;
	
	@JsonProperty("Application") 
	List<Applications> appList;
	
	@JsonProperty("Device") 
	List<Device> deviceList;

	public List<Model> getModelList() {
		return modelList;
	}

	public void setModelList(List<Model> modelList) {
		this.modelList = modelList;
	}

	public List<Manufacturer> getManufacturerList() {
		return manufacturerList;
	}

	public void setManufacturerList(List<Manufacturer> manufacturerList) {
		this.manufacturerList = manufacturerList;
	}

	public List<Applications> getAppList() {
		return appList;
	}

	public void setAppList(List<Applications> appList) {
		this.appList = appList;
	}

	public List<Device> getDeviceList() {
		return deviceList;
	}

	public void setDeviceList(List<Device> deviceList) {
		this.deviceList = deviceList;
	}


	
	
	
	

	
	
}

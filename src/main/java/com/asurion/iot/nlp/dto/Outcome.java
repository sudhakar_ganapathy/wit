package com.asurion.iot.nlp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Outcome {
	@JsonProperty("entities") 
	private Entity entity;
	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	private String intent;

	public String getIntent() {
		return intent;
	}

	public void setIntent(String intent) {
		this.intent = intent;
	}

	
	
	

}

package com.asurion.iot.nlp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Model {
	@JsonProperty("value") 
	private String value;
	
	@JsonProperty("metadata") 
	private String metadata;
	
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

}

package com.asurion.iot.nlp.dto;

import java.util.List;

public class IntentInfo {

	
	private String intent;
	private String device;
	private String manufacturer;
	private String model;
	List<String> appNames;
	
	public String getIntent() {
		return intent;
	}
	public void setIntent(String intent) {
		this.intent = intent;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public List<String> getAppNames() {
		return appNames;
	}
	public void setAppNames(List<String> appNames) {
		this.appNames = appNames;
	}
}

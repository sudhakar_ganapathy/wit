package com.asurion.iot.nlp.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WitResponseWrapper {
	
	private String msg_id;
	private String _text;
	@JsonProperty("outcomes") 
	private List<Outcome> outcomes;
	
		
	public List<Outcome> getOutcomes() {
		return outcomes;
	}
	public void setOutcomes(List<Outcome> outcomes) {
		this.outcomes = outcomes;
	}
	public String getMsg_id() {
		return msg_id;
	}
	public void setMsg_id(String msg_id) {
		this.msg_id = msg_id;
	}
	public String get_text() {
		return _text;
	}
	public void set_text(String _text) {
		this._text = _text;
	}
	

}

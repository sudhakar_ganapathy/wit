package com.asurion.iot.nlp.dto;

public class Manufacturer {
	
	private String value;
	private String metadata;
	
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

}
